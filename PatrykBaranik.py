from random import randint
import pygame, time

   
class Pacman:
  def __init__(self):
    self.wier=None
    self.kol=None
    self.wyn=None
    self.pkt=0
    self.kier=None
    self.tab=[]

  def Plansza1(self, wys, szer):
    tab = [None]*wys
    for x in range (wys):
        tab[x]=[9]+[1]+[9]*(szer-4)+[1]+[9]
    tab[0]=[9]*szer
    tab[wys-1]=[9]*szer
    tab[1]=[9]+[1]*(szer-2)+[9]
    tab[wys-2]=[9]+[1]*(szer-2)+[9]
    self.tab=tab
 
  def Plansza2(self):
    self.tab=[[9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,9,9,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,1,1,9],
     [9,9,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,1,1,1,1,1,1,1,1,1,1,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,1,1,1,1,1,1,1,1,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,9,9,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,9,9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9,9,1,1,1,9],
     [9,9,9,1,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,1,9,9,9],
     [9,9,9,1,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,1,9,9,9],
     [9,1,1,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,9,9,1,9],
     [9,1,9,9,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9]]
    
    self.wier=1
    self.kol=1
    self.wyn=1
    self.pkt=316
    self.kier='<'
    self.tab[self.wier][self.kol]=self.kier
   
  def Plansza3(self):
    self.tab=[[9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,9,9,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,1,1,9],
     [9,9,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,1,1,1,1,1,1,1,1,1,1,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9],
     [9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,1,1,1,1,1,1,1,1,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,9,9,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,9,9,9,9,1,9,9,9,9,9,1,9,9,1,9,9,9,9,9,1,9,9,9,9,1,9],
     [9,1,1,1,9,9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9,9,1,1,1,9],
     [9,9,9,1,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,1,9,9,9],
     [9,9,9,1,9,9,1,9,9,1,9,9,9,9,9,9,9,9,1,9,9,1,9,9,1,9,9,9],
     [9,1,1,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,9,9,1,1,1,1,1,1,9],
     [9,1,9,9,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,9,9,1,9],
     [9,1,9,9,9,9,9,9,9,9,9,9,1,9,9,1,9,9,9,9,9,9,9,9,9,9,1,9],
     [9,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,9],
     [9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9,9]]
 
    self.wier=1
    self.kol=1
    self.wyn=1
    self.pkt=316
    self.kier='<'
    self.tab[self.wier][self.kol]=self.kier

  def start(self, wiersz, kolumna, pkt, kier):
    self.wier=int(wiersz)
    self.kol=int(kolumna)
    self.wyn=1
    self.pkt=pkt
    self.kier=kier
    self.tab[wiersz][kolumna]=kier
 
  def zmiana(self, nKier):
 
    if nKier==273:
      self.kier='v'
      self.tab[self.wier][self.kol]=self.kier
   
    if nKier==274:
      self.kier='^'
      self.tab[self.wier][self.kol]=self.kier
   
    if nKier==276:
      self.kier='>'
      self.tab[self.wier][self.kol]=self.kier
   
    if nKier==275:
      self.kier='<'
      self.tab[self.wier][self.kol]=self.kier
 
  def ruch(self):
      tab=self.tab
     
      if self.kier=='<':
        if tab[self.wier][self.kol+1]==0 or tab[self.wier][self.kol+1]==1:
          if tab[self.wier][self.kol+1]==1:
            self.wyn+=1
          tab[self.wier][self.kol]=0
          self.kol=self.kol+1
          tab[self.wier][self.kol]='<'
         
        elif tab[self.wier][self.kol+1]==9:
          self.kier='>'
          self.ruch
         
        elif tab[self.wier][self.kol+1]==7:
          return 7
         
      elif self.kier=='>':
        if tab[self.wier][self.kol-1]==0 or tab[self.wier][self.kol-1]==1:
          if tab[self.wier][self.kol-1]==1:
            self.wyn+=1
          tab[self.wier][self.kol]=0
          self.kol=self.kol-1
          tab[self.wier][self.kol]='>'
         
        elif tab[self.wier][self.kol-1]==9:
          self.kier='<'
          self.ruch
         
        elif tab[self.wier][self.kol-1]==7:
          return 7
         
      elif self.kier=='^':
        if tab[self.wier+1][self.kol]==0 or tab[self.wier+1][self.kol]==1:
          if tab[self.wier+1][self.kol]==1:
            self.wyn+=1
          tab[self.wier][self.kol]=0
          self.wier=self.wier+1
          tab[self.wier][self.kol]='^' 
       
        elif tab[self.wier+1][self.kol]==9:
          self.kier='v'
          self.ruch
         
        elif tab[self.wier+1][self.kol]==7:
          return 7
     
      elif self.kier=='v':
        if tab[self.wier-1][self.kol]==0 or tab[self.wier-1][self.kol]==1:
          if tab[self.wier-1][self.kol]==1:
            self.wyn+=1
          tab[self.wier][self.kol]=0
          self.wier=self.wier-1
          tab[self.wier][self.kol]='v'
         
        elif tab[self.wier-1][self.kol]==9:
          self.kier='^'
          self.ruch
         
        elif tab[self.wier-1][self.kol]==7:
          return 7
         
         
class Ghost():
  def __init__(self):
    self.wier=None
    self.kol=None
    self.kier=None
    self.osob=None
    self.pod=1
    self.plus=0
 
  def startduch(self,wier,kol,kier,tab):
    self.wier=wier
    self.kol=kol
    self.kier=kier
    tab[wier][kol]=7
 
  def ruchduch(self,tab):
   
      if tab[self.wier][self.kol]==0:
        self.plus+=1
        
        
      if self.pod==7:
          self.pod=1
      
   
      if self.kier=='<':
        self.osob=randint(3,4)
      elif self.kier=='>':
        self.osob=randint(3,4)
      elif self.kier=='^':
        self.osob=randint(1,2)
      elif self.kier=='v':
        self.osob=randint(1,2)

   
      if self.osob==1:
        if tab[self.wier][self.kol+1]!=9:
          self.kier = '<'
      elif self.osob==2:
        if tab[self.wier][self.kol-1]!=9:
          self.kier = '>'
      elif self.osob==3:
        if tab[self.wier+1][self.kol]!=9:
          self.kier = '^'
      elif self.osob==4:
        if tab[self.wier-1][self.kol]!=9:
          self.kier = 'v'
   
      if self.kier=='<':
        if tab[self.wier][self.kol+1]==0 or tab[self.wier][self.kol+1]==1:
          tab[self.wier][self.kol]=self.pod
          if tab[self.wier][self.kol+1]==1:
            self.pod=1
          elif tab[self.wier][self.kol+1]==0:
            self.pod=0
          self.kol=self.kol+1
          tab[self.wier][self.kol]=7

       
        elif tab[self.wier][self.kol+1]=='<' or tab[self.wier][self.kol+1]=='>' or tab[self.wier][self.kol+1]=='^' or tab[self.wier][self.kol+1]=='v':
          return 7
         
        elif tab[self.wier][self.kol+1]==7:
          self.pod=1
          self.kol=self.kol+1
          tab[self.wier][self.kol]=7

        elif tab[self.wier][self.kol+1]==9:
          self.ruchduch(tab)
         
      elif self.kier=='>':
        if tab[self.wier][self.kol-1]==0 or tab[self.wier][self.kol-1]==1:
          tab[self.wier][self.kol]=self.pod
          if tab[self.wier][self.kol-1]==1:
            self.pod=1
          elif tab[self.wier][self.kol-1]==0:
            self.pod=0
          self.kol=self.kol-1
          tab[self.wier][self.kol]=7
         
        elif tab[self.wier][self.kol-1]=='<' or tab[self.wier][self.kol-1]=='>' or tab[self.wier][self.kol-1]=='^' or tab[self.wier][self.kol-1]=='v':
          return 7
         
        elif tab[self.wier][self.kol-1]==7:
          self.pod=1
          self.kol=self.kol-1
          tab[self.wier][self.kol]=7
         
        elif tab[self.wier][self.kol-1]==9:
          self.ruchduch(tab)
         
      elif self.kier=='^':
        if tab[self.wier+1][self.kol]==0 or tab[self.wier+1][self.kol]==1:
          tab[self.wier][self.kol]=self.pod
          if tab[self.wier+1][self.kol]==1:
            self.pod=1
          elif tab[self.wier+1][self.kol]==0:
            self.pod=0
          self.wier=self.wier+1
          tab[self.wier][self.kol]=7
         
        elif tab[self.wier+1][self.kol]=='<' or tab[self.wier+1][self.kol]=='>' or tab[self.wier+1][self.kol]=='^' or tab[self.wier+1][self.kol]=='v':
          return 7
         
        elif tab[self.wier+1][self.kol]==7:
          self.pod=1
          self.wier=self.wier+1
          tab[self.wier][self.kol]=7
         
        elif tab[self.wier+1][self.kol]==9:
          self.ruchduch(tab)
     
      elif self.kier=='v':
        if tab[self.wier-1][self.kol]==0 or tab[self.wier-1][self.kol]==1:
          tab[self.wier][self.kol]=self.pod
          if tab[self.wier-1][self.kol]==1:
            self.pod=1
          elif tab[self.wier-1][self.kol]==0:
            self.pod=0
          self.wier=self.wier-1
          tab[self.wier][self.kol]=7
         
        elif tab[self.wier-1][self.kol]=='<' or tab[self.wier-1][self.kol]=='>' or tab[self.wier-1][self.kol]=='^' or tab[self.wier-1][self.kol]=='v':
          return 7
         
        elif tab[self.wier-1][self.kol]==7:
          self.pod=1
          self.wier=self.wier-1
          tab[self.wier][self.kol]=7
         
        elif tab[self.wier-1][self.kol]==9:
          self.ruchduch(tab)



w = Pacman()
w.Plansza1(9,9)
w.tab[2]=[9,1,9,1,9,9,9,1,9]
w.tab[3]=[9,1,1,1,1,1,9,1,9]
w.tab[4]=[9,1,9,1,9,1,9,1,9]
w.tab[5]=[9,1,9,1,1,1,1,1,9]
w.tab[6]=[9,1,9,9,9,1,9,1,9]
w.start(1,4,36,'^')
f = Ghost()
s = Ghost()
t = Ghost()
f.startduch(7,7,'>',w.tab)
s.startduch(1,7,'<',w.tab)
t.startduch(7,1,'^',w.tab)






w.Plansza2()
f.startduch(29,1,'>',w.tab)
s.startduch(29,26,'<',w.tab)
t.startduch(1,26,'^',w.tab)




pygame.init()
screen = pygame.display.set_mode((840, 930))

sciana = pygame.image.load("sciana.jpg")
puste = pygame.image.load("puste.jpg")
punkt = pygame.image.load("punkt.jpg")
ghost = pygame.image.load("ghost.jpg")
gora=pygame.image.load("gora.jpg")
dol=pygame.image.load("dol.jpg")
pr=pygame.image.load("pr.jpg")
lw=pygame.image.load("lw.jpg")
za=pygame.image.load("za.jpg")
g=pygame.image.load("g.jpg")
d=pygame.image.load("d.jpg")
p=pygame.image.load("p.jpg")
l=pygame.image.load("l.jpg")

running = True
while running:

    start_time = time.time()
    while (time.time()-start_time<0.002):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                w.zmiana(event.key)

    for j in range (0,28):
        for i in range (0,31):
            if w.tab[i][j]==9:
                screen.blit(sciana, (j*30,i*30))
            if w.tab[i][j]==0:
                screen.blit(puste, (j*30,i*30))
            elif w.tab[i][j]==1:
                screen.blit(punkt, (j*30,i*30))
            elif w.tab[i][j]==7:
                screen.blit(ghost, (j*30,i*30))
            elif w.tab[i][j]=='^':
                screen.blit(dol, (j*30,i*30))
            elif w.tab[i][j]=='v':  
                screen.blit(gora, (j*30,i*30))
            elif w.tab[i][j]=='<': 
                screen.blit(pr, (j*30,i*30))
            elif w.tab[i][j]=='>':
                screen.blit(lw, (j*30,i*30))
    
    pygame.display.flip()
    
    
    if w.ruch()==7 or f.ruchduch(w.tab)==7 or s.ruchduch(w.tab)==7 or t.ruchduch(w.tab)==7:
        print ("Przegrana")
        running=False

    start_time = time.time()
    while (time.time()-start_time<0.002):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                w.zmiana(event.key) 

    for j in range (0,28):
        for i in range (0,31):
            if w.tab[i][j]==9:
                screen.blit(sciana, (j*30,i*30))
            if w.tab[i][j]==0:
                screen.blit(puste, (j*30,i*30))
            elif w.tab[i][j]==1:
                screen.blit(punkt, (j*30,i*30))
            elif w.tab[i][j]==7:
                screen.blit(ghost, (j*30,i*30))
            elif w.tab[i][j]=='^':
                screen.blit(d, (j*30,i*30))
            elif w.tab[i][j]=='v':  
                screen.blit(g, (j*30,i*30))
            elif w.tab[i][j]=='<': 
                screen.blit(p, (j*30,i*30))
            elif w.tab[i][j]=='>':
                screen.blit(l, (j*30,i*30))
    
    pygame.display.flip()

    start_time = time.time()
    while (time.time()-start_time<0.002):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                running = False
            elif event.type == pygame.KEYDOWN:
                w.zmiana(event.key) 

    for j in range (0,28):
        for i in range (0,31):
            if w.tab[i][j]==9:
                screen.blit(sciana, (j*30,i*30))
            if w.tab[i][j]==0:
                screen.blit(puste, (j*30,i*30))
            elif w.tab[i][j]==1:
                screen.blit(punkt, (j*30,i*30))
            elif w.tab[i][j]==7:
                screen.blit(ghost, (j*30,i*30))
            elif w.tab[i][j]=='^':
                screen.blit(za, (j*30,i*30))
            elif w.tab[i][j]=='v':  
                screen.blit(za, (j*30,i*30))
            elif w.tab[i][j]=='<': 
                screen.blit(za, (j*30,i*30))
            elif w.tab[i][j]=='>':
                screen.blit(za, (j*30,i*30))
    
    pygame.display.flip()

    
    if w.wyn==w.pkt-f.plus-s.plus-t.plus:
        print ("Wygrana")
        running=False
pygame.quit()

